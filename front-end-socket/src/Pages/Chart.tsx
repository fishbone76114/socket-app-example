import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { socket } from '../socket/socket';
import { ChartDataType } from '../Interfaces/chart-types';
import PieChart from '../Component/PieChart';

const Chart = () => {
  const [data, setData] = useState<ChartDataType | any>();

  useEffect(() => {
    socket.on('chart-data-dashboard', (dt) => {
      setData(dt);
    });
  }, []);

  return (
    <div className="w-25 mx-auto">
      <div>{data && <PieChart chartData={data} />}</div>
      <div>
        <Link to="/">Back to Home</Link>
      </div>
    </div>
  );
};

export default Chart;
