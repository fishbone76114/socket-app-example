import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { socket } from '../socket/socket';

const Layout = () => {
  useEffect(() => {
    socket.emit('reset-counter');
  }, []);

  return (
    <div className="mx-auto w-25">
      <h1>Welcome Home</h1>
      <ul style={{ listStyle: 'none' }}>
        <li>
          <Link to="/user">{'>>'} User Page</Link>
        </li>
        <li>
          <Link to="/chart">{'>>'} Chart Dashboard</Link>
        </li>
      </ul>
    </div>
  );
};

export default Layout;
