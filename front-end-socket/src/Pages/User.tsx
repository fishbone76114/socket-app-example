import React, { useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { socket } from '../socket/socket';

const User = () => {
  useEffect(() => {
    socket.emit('reset-counter');
  }, []);

  const hanldeColorClick = (color: string) => {
    socket.emit('client-color-trigger', color);
  };

  return (
    <div className="w-25 mx-auto">
      <h3>Click to update Graph</h3>
      <Button
        className="me-1 w-25"
        variant="primary"
        onClick={() => hanldeColorClick('BLUE')}
      >
        Blue
      </Button>
      <Button
        className="me-1 w-25"
        variant="warning"
        onClick={() => hanldeColorClick('ORANGE')}
      >
        Orange
      </Button>
      <p>
        <Link to="/">Back to Home</Link>
      </p>
    </div>
  );
};

export default User;
