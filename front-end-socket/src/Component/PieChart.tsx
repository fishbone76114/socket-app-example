import React from 'react';
import { Pie } from 'react-chartjs-2';
import { ChartDataType } from '../Interfaces/chart-types';
import { CategoryScale } from 'chart.js';
import Chart from 'chart.js/auto';

Chart.register(CategoryScale);

const PieChart = ({ chartData }: { chartData: ChartDataType }) => {
  return (
    <div className="chart-container">
      <h2 style={{ textAlign: 'center' }}>Pie Chart</h2>
      <Pie
        data={chartData}
        options={{
          plugins: {
            title: {
              display: true,
              text: 'User Click Counter',
            },
          },
        }}
      />
    </div>
  );
};
export default PieChart;
