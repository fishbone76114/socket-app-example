interface DataSetType {
  label: string;
  data: number[];
  backgroundColor: string[];
  hoverOffset: number;
}

export interface ChartDataType {
  labels: string[];
  datasets: DataSetType[];
}
