import { Test } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EventsModule } from './events/events.module';

describe('AppModule', () => {
  let app;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [EventsModule],
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  describe('imports', () => {
    it('should import the EventsModule', () => {
      expect(app.get(EventsModule)).toBeDefined();
    });
  });

  describe('controllers', () => {
    it('should create the AppController', () => {
      expect(app.get(AppController)).toBeDefined();
    });
  });

  describe('providers', () => {
    it('should create the AppService', () => {
      expect(app.get(AppService)).toBeDefined();
    });
  });
});
