import { Interval } from '@nestjs/schedule';
import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server } from 'socket.io';

@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class EventsGateway {
  @WebSocketServer()
  server: Server;
  COLOR: { BLUE: number; ORANGE: number } = {
    BLUE: 0,
    ORANGE: 0,
  };

  @SubscribeMessage('client-color-trigger')
  setData(@MessageBody() data: string) {
    if (data) {
      this.COLOR[data] += 1;
    }
  }

  @SubscribeMessage('reset-counter')
  resetCounter() {
    this.COLOR = {
      BLUE: 0,
      ORANGE: 0,
    };
  }

  @Interval(5000)
  handleIntervalForChart() {
    const { BLUE, ORANGE } = this.COLOR;
    const dataSet = {
      labels: ['BLUE', 'ORANGE'],
      datasets: [
        {
          label: 'User Click Counter',
          data: [BLUE, ORANGE],
          backgroundColor: ['#0D6EFD', '#FFC107'],
          hoverOffset: 4,
        },
      ],
    };

    this.server.sockets.emit('chart-data-dashboard', dataSet);
  }
}
