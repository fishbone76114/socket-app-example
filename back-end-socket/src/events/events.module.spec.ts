import { Test } from '@nestjs/testing';
import { ScheduleModule } from '@nestjs/schedule';
import { EventsGateway } from './events.gateway';
import { EventsModule } from './events.module';

describe('EventsModule', () => {
  let eventsGateway: EventsGateway;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [ScheduleModule.forRoot(), EventsModule],
    }).compile();

    eventsGateway = moduleRef.get<EventsGateway>(EventsGateway);
  });

  it('should be defined', () => {
    expect(eventsGateway).toBeDefined();
  });
});
