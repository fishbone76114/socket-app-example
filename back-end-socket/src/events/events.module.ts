import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { EventsGateway } from './events.gateway';

@Module({
  imports: [ScheduleModule.forRoot()],
  providers: [EventsGateway],
})
export class EventsModule {}
