import { Test, TestingModule } from '@nestjs/testing';
import { Server } from 'socket.io';
import { EventsGateway } from './events.gateway';

describe('EventsGateway', () => {
  let gateway: EventsGateway;
  let socket: Server;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      providers: [EventsGateway],
    }).compile();

    gateway = app.get<EventsGateway>(EventsGateway);
  });

  describe('setData', () => {
    it('should increment BLUE color count', () => {
      const initialColorCount = gateway.COLOR.BLUE;
      gateway.setData('BLUE');
      expect(gateway.COLOR.BLUE).toBe(initialColorCount + 1);
    });

    it('should increment ORANGE color count', () => {
      const initialColorCount = gateway.COLOR.ORANGE;
      gateway.setData('ORANGE');
      expect(gateway.COLOR.ORANGE).toBe(initialColorCount + 1);
    });

    it('should not increment count if data is falsy', () => {
      const initialColorCount = gateway.COLOR.BLUE;
      gateway.setData('');
      expect(gateway.COLOR.BLUE).toBe(initialColorCount);
    });
  });

  describe('resetCounter', () => {
    it('should reset BLUE and ORANGE counts to 0', () => {
      gateway.COLOR.BLUE = 10;
      gateway.COLOR.ORANGE = 20;
      gateway.resetCounter();
      expect(gateway.COLOR.BLUE).toBe(0);
      expect(gateway.COLOR.ORANGE).toBe(0);
    });
  });

  describe('handleIntervalForChart', () => {
    it('should emit chart-data-dashboard event with correct data set', () => {
      const BLUE = 5;
      const ORANGE = 10;
      gateway.COLOR.BLUE = BLUE;
      gateway.COLOR.ORANGE = ORANGE;

      const expectedDataSet = {
        labels: ['BLUE', 'ORANGE'],
        datasets: [
          {
            label: 'User Click Counter',
            data: [BLUE, ORANGE],
            backgroundColor: ['#0D6EFD', '#FFC107'],
            hoverOffset: 4,
          },
        ],
      };

      socket = new Server();
      gateway.server = socket;

      const spy = jest.spyOn(socket.sockets, 'emit');

      gateway.handleIntervalForChart();

      expect(spy).toHaveBeenCalledWith('chart-data-dashboard', expectedDataSet);
    });
  });
});
